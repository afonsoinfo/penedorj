//
// ROUTE AND TEMPLATE CONTROL
//
var penedorj = angular
    .module('penedorj', ['mongolab'])
    .config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/restaurantes', {controller:RestaurantsCtrl, templateUrl:'scripts/modules/restaurants/html/restaurants.html'})
        .otherwise({redirectTo:'/restaurantes'});
    }]);

//
// DDD FILTER
//
penedorj.filter("ddd", function(){
	return function(text){
		return "(24) " + text;
	}
});

//
// SHARED DATA
//
penedorj.factory("SharedData", function() {
  return {
  	isLoading: false
  };
});