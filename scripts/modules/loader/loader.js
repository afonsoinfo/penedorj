//
// LOADER CONTROLLER
//
function LoaderCtrl($scope, SharedData){
	var self = this;
	$scope.sharedData = SharedData;
}

//
// DEPENDENCIY INJECTION
//
LoaderCtrl.$inject = ['$scope', 'SharedData'];