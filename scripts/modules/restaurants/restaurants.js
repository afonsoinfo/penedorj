//
// LIST TEMPLATE CONTROLLER
//
function RestaurantsCtrl($scope, RestaurantAPI, SharedData) {
	var self = this;
	$scope.sharedData = SharedData;
	$scope.restaurants = undefined;
	//$scope.isRestaurantsLoading = false;

	$scope.$watch('restaurants', function(newValue, oldValue) {
    	// console.log(newValue);
    	// console.log(oldValue);
   	});

	self.startModule = function(){
		//$scope.isRestaurantsLoading = true;
		$scope.restaurants = RestaurantAPI.query();
	}

	self.startModule();
}

RestaurantsCtrl.$inject = ['$scope', 'RestaurantAPI', 'SharedData'];