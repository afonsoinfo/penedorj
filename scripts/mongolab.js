// This is a module for cloud persistance in mongolab - https://mongolab.com
angular.module('mongolab', ['ngResource']).
  factory('RestaurantAPI', function($resource) {
    var RestaurantAPI = $resource('https://api.mongolab.com/api/1/databases' +
        '/penedorj/collections/restaurant/:id',
        { apiKey: 'tpmM9GgFXe1Zj7Z5mAKkl9_Jgy3yXCUr' }, {
          update: { method: 'PUT' }
        }
    );

    RestaurantAPI.prototype.update = function(cb) {
      return RestaurantAPI.update({id: this._id.$oid},
          angular.extend({}, this, {_id:undefined}), cb);
    };

    RestaurantAPI.prototype.destroy = function(cb) {
      return RestaurantAPI.remove({id: this._id.$oid}, cb);
    };

    return RestaurantAPI;
  });