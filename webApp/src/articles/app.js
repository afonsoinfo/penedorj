
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path');

var app = express();
app.set('title', 'Penedo RJ');

// DATA ABSTRACTION LAYER
// var ArticleProvider = require('./articleprovider-memory').ArticleProvider;
var ArticleProvider = require('./articleprovider-mongodb').ArticleProvider;
//var articleProvider= new ArticleProvider();
var articleProvider = new ArticleProvider('localhost', 27017);

// DUMMY DATA
function User(name, email) {
  this.name = name;
  this.email = email;
}

var users = [
    new User('tj', 'tj@vision-media.ca')
  , new User('ciaran', 'ciaranj@gmail.com')
  , new User('aaron', 'aaron.heckmann+github@gmail.com')
];

// SETTINGS
app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);

app.get('/malucada', function(req, res){
  res.render('malucada', { users: users });
});

app.get('/users', user.list);

app.get('/articles', function(req, res){
  
       articleProvider.findAll( function(error,docs){
        res.render('articles', { data: {
            title: 'Artigos Escritos',
            articles:docs
          }
        });
    })
});

app.get('/articles/new', function(req, res) {
    res.render('new_article', { data: {
        title: 'Novo Artigo'
    }
    });
});

app.post('/articles/new', function(req, res){
    articleProvider.save({
        title: req.param('title'),
        body: req.param('body')
    }, function( error, docs) {
        res.redirect('/articles')
    });
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
