var Db = require('mongodb').Db;
var Connection = require('mongodb').Connection;
var Server = require('mongodb').Server;
var BSON = require('mongodb').BSON;
var ObjectID = require('mongodb').ObjectID;

RestaurantProvider = function(host, port) {
  this.db = new Db('node-mongo-blog', new Server(host, port, {auto_reconnect: true}, {}));
  this.db.open(function(){});
};

//getCollection

RestaurantProvider.prototype.getCollection= function(callback) {
  this.db.collection('restaurants', function(error, restaurant_collection) {
    if (error) {
      callback(error);
    } else {
      callback(null, restaurant_collection);
    }
  });
};

//findAll
RestaurantProvider.prototype.findAll = function(callback) {
    this.getCollection(function(error, restaurant_collection) {
      if (error) {
        callback(error);
      }
      else {
        restaurant_collection.find().toArray(function(error, results) {
          if(error) {
            callback(error);
          } else {
            callback(null, results);
          }
        });
      }
    });
};

//findById

RestaurantProvider.prototype.findById = function(id, callback) {
    this.getCollection(function(error, restaurant_collection) {
      if (error) {
        callback(error);
      } else {
        restaurant_collection.findOne({_id: restaurant_collection.db.bson_serializer.ObjectID.createFromHexString(id)}, function(error, result) {
          if (error) {
            callback(error);
          } else {
            callback(null, result);
          }
        });
      }
    });
};

//save
RestaurantProvider.prototype.save = function(restaurants, callback) {
    this.getCollection(function(error, restaurant_collection) {
      if (error) {
        callback(error);
      } else {
        if (typeof(restaurants.length) == "undefined"){
          restaurants = [restaurants];
        }

        for( var i = 0; i < restaurants.length; i++) {
          restaurant = restaurants[i];
          restaurant.created_at = new Date();
          if( restaurant.comments === undefined ) restaurant.comments = [];
          for(var j =0;j< restaurant.comments.length; j++) {
            restaurant.comments[j].created_at = new Date();
          }
        }

        restaurant_collection.insert(restaurants, function() {
          callback(null, restaurants);
        });
      }
    });
};

exports.RestaurantProvider = RestaurantProvider;
